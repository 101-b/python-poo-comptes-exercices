"""
user bank interface
"""
import AccountClasses
from CONST import *

current_account = AccountClasses.CompteCourant
saving_account = AccountClasses.CompteEpargne

"""
choix du type et création du compte
"""
type_de_compte = input(
    "CC : 1 \n"
    "CE : 2 \n")
if type_de_compte == "1":
    compte = current_account(CURRENT_ACCOUNT_NUMBER, CLIENT_NAME, BALANCE, MIN, AGIOS)
elif type_de_compte == "2":
    compte = saving_account(SAVING_ACCOUNT_NUMBER, CLIENT_NAME, BALANCE, INTEREST)
else:
    raise Exception('entrées possibles : 1 ou 2')

"""
choix de l'action
"""
while True:
    if type_de_compte == "1":
        compte.appliquerAgios()
        print(f"Compte n° {CURRENT_ACCOUNT_NUMBER}, client : {CLIENT_NAME}\n"
              f"#########################")
    elif type_de_compte == "2":
        compte.appliquerInterets()
        print(f"Compte n° {SAVING_ACCOUNT_NUMBER}, client : {CLIENT_NAME}\n"
              f"#########################")

    action = input(
                   "1 : afficher le solde \n"
                   "2 : retrait \n"
                   "3 : dépôt \n"
    )

    # 1 : afficher le solde
    if action == "1":
        print("Solde actuel : ", round(compte.afficherSolde(), 2))

    # 2 : retraits
    elif action == "2":
        print("Solde actuel : ", round(compte.afficherSolde(), 2))
        amount = float(input("Montant du retrait :"))
        if amount < 0:
            raise Exception('Entrez un montant positif')

        # retrait compte courant
        elif type_de_compte == "1" and compte.afficherSolde() + MIN >= amount:
            compte.retrait(amount)
            print(f"Nouveau solde : {round(compte.afficherSolde(),2)}")
        elif type_de_compte == "1" and compte.afficherSolde() + MIN < amount:
            print(f"opération impossible (retrait max : {round(compte.afficherSolde() + MIN,2)})")

        # retrait compte épargne
        elif type_de_compte == "2" and compte.afficherSolde() >= amount:
            compte.retrait(amount)
            print(f"Nouveau solde : {round(compte.afficherSolde(), 2)}")
        elif type_de_compte == "2" and compte.afficherSolde() < amount:
            print(f"opération impossible (retrait max : {round(compte.afficherSolde(),2)}")

        # saisie invalide
        else:
            raise Exception('entrées possibles : nombres positifs')

    # 3 : versement
    elif action == "3":
        print("Solde actuel : ", round(compte.afficherSolde(), 2))
        amount = float(input("montant du dépôt : "))
        if amount < 0:
            raise Exception('Entrez un montant positif')
        else:
            compte.versement(amount)
            print("Nouveau solde : ", round(compte.afficherSolde(), 2))

    # saisies invalides
    else:
        raise Exception('entrées possibles : 1, 2, 3, 4')

    # fin
    choice = input(
        "##############################\n"
        "[Q] pour sortir et récupérer la carte, ou [enter] pour continuer : \n"
    )
    if choice == "Q":
        print("Reprenez votre carte...")
        break
