"""
implémente en POO un fonctionnement bancaire basique
"""
from abc import ABC

"""
abstract class implements basic account attributes
"""
class Compte(ABC):
    def __init__(self, numeroCompte: int, nomProprietaire: str, solde: float):
        self.numeroCompte = numeroCompte
        self.solde = solde
        self.nomProprietaire = nomProprietaire

    """
    substracts an amount and return the account balance
    """
    def retrait(self, amount: float) -> float:
        self.solde -= amount
        return self.solde

    """
    add an amount on the account balance
    """
    def versement(self, amount: float) -> float:
        self.solde += amount
        return self.solde

    """
    return the account balance
    """
    def afficherSolde(self) -> float:
        return round(self.solde, 2)

    """
    current_account class
    """
class CompteCourant(Compte):
    def __init__(self, numeroCompte, nomProprietaire, solde, autorisationDecouvert:float, pourcentageAgios:float):
        super().__init__(numeroCompte, nomProprietaire, solde)
        self.__autorisationDecouvert = autorisationDecouvert
        self.__pourcentageAgios = pourcentageAgios

    """
    apply agios on the account balance if balance < 0
    """
    def appliquerAgios(self):
        agios_factor = 1 + self.__pourcentageAgios
        if self.solde < 0:
            self.solde *= agios_factor
        return self.solde


"""
saving_account class
"""
class CompteEpargne(Compte):
    def __init__(self, numeroCompte, nomProprietaire, solde, pourcentageInterets:float):
        super().__init__(numeroCompte, nomProprietaire, solde)
        self.__pourcentageInterets = pourcentageInterets
    """
    apply interests on account balance if balance > 0
    """
    def appliquerInterets(self):
        interest_factor = 1 + self.__pourcentageInterets
        if self.solde > 0:
            self.solde *= interest_factor
        return self.solde
