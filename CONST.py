"""
user bank interface CONST
"""
BALANCE = 1000
AGIOS = 0.01  # appliqué à chaque consultation
INTEREST = 0.01  # appliqué à chaque consultation
CLIENT_NAME = "Manu Chao"
MIN = 500  # découvert max
CURRENT_ACCOUNT_NUMBER = 1234
SAVING_ACCOUNT_NUMBER = 4321